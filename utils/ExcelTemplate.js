sap.ui.define(['sap/ui/base/Object'],
    function (Object) {

        /**
         * @class ExcelTemplate - абстрактный класс для шаблона Excel
         * @extends sap.ui.base.Object
         * @author Artem Maltsev
         * @abstract
         * @public
         * @alias suek.test1.utils.ExcelTemplate
         */
        var ExcelTemplate = Object.extend("suek.test1.utils.ExcelTemplate", {
            constructor: function(template) {
                this.template = template;
                this.content = "";
            }
        });


        /**
         * Абстрактный метод заполнения шаблона
         * @abstract
         * @param data
         */
        ExcelTemplate.prototype.fillTemplate = function (data) {
            this.content = this.template;
        };

        /**
         * Сохранить файл
         * @param filename
         */
        ExcelTemplate.prototype.save = function (filename) {
            download(this.content, filename + '.xml', "application/vnd.ms-excel");
        };

        /**
         * Заполнить шаблон и сохранить файл
         * @param data
         * @param filename
         */
        ExcelTemplate.prototype.generateAndSave = function (data, filename) {
            this.fillTemplate(data);
            this.save(filename);
        };


        return ExcelTemplate;


        function download(data, filename, type) {
            var file = new Blob([data], {type: type});
            if (window.navigator.msSaveOrOpenBlob) // IE10+
                window.navigator.msSaveOrOpenBlob(file, filename);
            else { // Others
                var a = document.createElement("a"),
                    url = URL.createObjectURL(file);
                a.href = url;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                setTimeout(function() {
                    document.body.removeChild(a);
                    window.URL.revokeObjectURL(url);
                }, 0);
            }
        }

    });