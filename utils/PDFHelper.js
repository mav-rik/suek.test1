sap.ui.define(['sap/ui/base/Object'],
    function (Object) {

        /**
         * @class PDF Helper - генерация PDF для наших данных
         * @extends sap.ui.base.Object
         * @author Artem Maltsev
         * @public
         * @alias suek.test1.utils.PDFHelper
         */
        var PDFHelper = Object.extend("suek.test1.utils.PDFHelper", {
            constructor: function(data, fields, docTypes, i18nModel) {
                this.data = data;
                this.fields = fields;
                this.docTypes = docTypes;
                this.i18n = i18nModel;
            }
        });

        /**
         * Создать заголовок раздела
         * @param name
         */
        PDFHelper.prototype.createTitle = function (name) {
            this.content.push({ text: this.i18n.getResourceBundle().getText(name), style: 'title' });
        };


        /**
         * Создать строку текста (лэйбл+значение)
         * @param label
         * @param text
         */
        PDFHelper.prototype.createText = function (label, text) {
            var labelText = this.i18n.getResourceBundle().getText(label) + ': ';
            this.content.push({ text: [{text: labelText, style: 'label'}, text], style: 'defaultStyle'});
        };


        /**
         * Сохранить файл
         * @param name
         */
        PDFHelper.prototype.save = function (name) {

            this.content = [];

            this.createTitle("name");
            for (var field in this.fields["name"]) {
                this.createText(field, this.data[field]);
            }


            this.createTitle("document");

            var docType = this.docTypes.find(function(dt) {
                return dt.key == this.data.docType
            }.bind(this));

            docType.fields.forEach(function(field) {
                this.createText(field, this.data[field])
            }.bind(this));



            this.createTitle("education");
            this.data.education.forEach(function(ed) {
                for (var field in this.fields["education"]) {
                    this.createText(field, ed[this.fields["education"][field].model]);
                }
            }.bind(this));


            this.pdf = pdfMake.createPdf({
                content: this.content,
                styles: {
                    title: {
                        fontSize: 16,
                        bold: true,
                        margin: [0, 10]
                    },
                    label: {
                        italics: true,
                        color: 'grey',
                        alignment: 'left'
                    },
                    defaultStyle: {
                        margin: [0, 2]
                    }
                }
            });

            this.pdf.getBlob(function(blob) {
                downloadBlob(blob, name + '.pdf');
            });
        };


        return PDFHelper;


        function downloadBlob(file, filename) {
            if (window.navigator.msSaveOrOpenBlob) // IE10+
                window.navigator.msSaveOrOpenBlob(file, filename);
            else { // Others
                var a = document.createElement("a"),
                    url = URL.createObjectURL(file);
                a.href = url;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                setTimeout(function() {
                    document.body.removeChild(a);
                    window.URL.revokeObjectURL(url);
                }, 0);
            }
        }

    });