sap.ui.define(['suek/test1/utils/ExcelTemplate'],
    function (ExcelTemplate) {

        /**
         * @class ExcelT1 - шаблон Excel для наших данных
         * @extends suek.test1.utils.ExcelTemplate
         * @author Artem Maltsev
         * @public
         * @alias suek.test1.utils.ExcelT1
         */
        var ExcelT1 = ExcelTemplate.extend("suek.test1.utils.ExcelT1", {
            constructor: function() {
                ExcelTemplate.call(this, "<?xml version=\"1.0\"?>\n" +
                    "<?mso-application progid=\"Excel.Sheet\"?>\n" +
                    "<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\n" +
                    " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
                    " xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\n" +
                    " xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"\n" +
                    " xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n" +
                    " <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">\n" +
                    "  <Author>Пользователь Windows</Author>\n" +
                    "  <LastAuthor>Пользователь Windows</LastAuthor>\n" +
                    "  <Created>2019-03-18T19:34:44Z</Created>\n" +
                    "  <Version>12.00</Version>\n" +
                    " </DocumentProperties>\n" +
                    " <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\n" +
                    "  <WindowHeight>12855</WindowHeight>\n" +
                    "  <WindowWidth>28755</WindowWidth>\n" +
                    "  <WindowTopX>0</WindowTopX>\n" +
                    "  <WindowTopY>30</WindowTopY>\n" +
                    "  <ProtectStructure>False</ProtectStructure>\n" +
                    "  <ProtectWindows>False</ProtectWindows>\n" +
                    " </ExcelWorkbook>\n" +
                    " <Styles>\n" +
                    "  <Style ss:ID=\"Default\" ss:Name=\"Normal\">\n" +
                    "   <Alignment ss:Vertical=\"Bottom\"/>\n" +
                    "   <Borders/>\n" +
                    "   <Font ss:FontName=\"Calibri\" x:CharSet=\"204\" x:Family=\"Swiss\" ss:Size=\"11\"\n" +
                    "    ss:Color=\"#000000\"/>\n" +
                    "   <Interior/>\n" +
                    "   <NumberFormat/>\n" +
                    "   <Protection/>\n" +
                    "  </Style>\n" +
                    "  <Style ss:ID=\"s66\">\n" +
                    "   <Alignment ss:Vertical=\"Bottom\"/>\n" +
                    "   <Font ss:FontName=\"Calibri\" x:CharSet=\"204\" x:Family=\"Swiss\" ss:Size=\"14\"\n" +
                    "    ss:Color=\"#000000\" ss:Bold=\"1\"/>\n" +
                    "  </Style>\n" +
                    " </Styles>\n" +
                    " <Worksheet ss:Name=\"Лист1\">\n" +
                    "  <Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"18\" x:FullColumns=\"1\"\n" +
                    "   x:FullRows=\"1\" ss:DefaultRowHeight=\"15\">\n" +
                    "   <Column ss:AutoFitWidth=\"0\" ss:Width=\"182.25\"/>\n" +
                    "   <Column ss:AutoFitWidth=\"0\" ss:Width=\"519\"/>\n" +
                    "   <Row ss:Height=\"18.75\">\n" +
                    "    <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s66\"><Data ss:Type=\"String\">Основные данные</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Фамилия </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%lastName%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Имя </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%firstName%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Отчетство </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%middleName%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Дата рождения </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%dob%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Возраст </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%age%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row ss:Index=\"8\" ss:Height=\"18.75\">\n" +
                    "    <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s66\"><Data ss:Type=\"String\">Документ</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Тип документа</Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%docType%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Номер паспорта </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%passportNumber%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Номер свидетельства </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%birthNumber%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Дата выдачи </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%issueDate%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Кем выдан </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%issuedBy%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row ss:Index=\"15\" ss:Height=\"18.75\">\n" +
                    "    <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s66\"><Data ss:Type=\"String\">Образование</Data></Cell>\n" +
                    "   </Row>\n" +
                    "  %educationTable% " +
                    "  </Table>\n" +
                    "  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n" +
                    "   <PageSetup>\n" +
                    "    <Header x:Margin=\"0.3\"/>\n" +
                    "    <Footer x:Margin=\"0.3\"/>\n" +
                    "    <PageMargins x:Bottom=\"0.75\" x:Left=\"0.7\" x:Right=\"0.7\" x:Top=\"0.75\"/>\n" +
                    "   </PageSetup>\n" +
                    "   <Print>\n" +
                    "    <ValidPrinterInfo/>\n" +
                    "    <PaperSizeIndex>0</PaperSizeIndex>\n" +
                    "    <VerticalResolution>0</VerticalResolution>\n" +
                    "    <NumberofCopies>0</NumberofCopies>\n" +
                    "   </Print>\n" +
                    "   <Selected/>\n" +
                    "   <Panes>\n" +
                    "    <Pane>\n" +
                    "     <Number>3</Number>\n" +
                    "     <RangeSelection>R1C1:R1C2</RangeSelection>\n" +
                    "    </Pane>\n" +
                    "   </Panes>\n" +
                    "   <ProtectObjects>False</ProtectObjects>\n" +
                    "   <ProtectScenarios>False</ProtectScenarios>\n" +
                    "  </WorksheetOptions>\n" +
                    " </Worksheet>\n" +
                    " <Worksheet ss:Name=\"Лист2\">\n" +
                    "  <Table ss:ExpandedColumnCount=\"1\" ss:ExpandedRowCount=\"1\" x:FullColumns=\"1\"\n" +
                    "   x:FullRows=\"1\" ss:DefaultRowHeight=\"15\">\n" +
                    "  </Table>\n" +
                    "  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n" +
                    "   <PageSetup>\n" +
                    "    <Header x:Margin=\"0.3\"/>\n" +
                    "    <Footer x:Margin=\"0.3\"/>\n" +
                    "    <PageMargins x:Bottom=\"0.75\" x:Left=\"0.7\" x:Right=\"0.7\" x:Top=\"0.75\"/>\n" +
                    "   </PageSetup>\n" +
                    "   <ProtectObjects>False</ProtectObjects>\n" +
                    "   <ProtectScenarios>False</ProtectScenarios>\n" +
                    "  </WorksheetOptions>\n" +
                    " </Worksheet>\n" +
                    " <Worksheet ss:Name=\"Лист3\">\n" +
                    "  <Table ss:ExpandedColumnCount=\"1\" ss:ExpandedRowCount=\"1\" x:FullColumns=\"1\"\n" +
                    "   x:FullRows=\"1\" ss:DefaultRowHeight=\"15\">\n" +
                    "  </Table>\n" +
                    "  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n" +
                    "   <PageSetup>\n" +
                    "    <Header x:Margin=\"0.3\"/>\n" +
                    "    <Footer x:Margin=\"0.3\"/>\n" +
                    "    <PageMargins x:Bottom=\"0.75\" x:Left=\"0.7\" x:Right=\"0.7\" x:Top=\"0.75\"/>\n" +
                    "   </PageSetup>\n" +
                    "   <ProtectObjects>False</ProtectObjects>\n" +
                    "   <ProtectScenarios>False</ProtectScenarios>\n" +
                    "  </WorksheetOptions>\n" +
                    " </Worksheet>\n" +
                    "</Workbook>\n");

                this.educationTableTemplate =  "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Начало </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%start%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Окончание </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%end%</Data></Cell>\n" +
                    "   </Row>\n" +
                    "   <Row>\n" +
                    "    <Cell><Data ss:Type=\"String\">Наименование учебного заведения </Data></Cell>\n" +
                    "    <Cell><Data ss:Type=\"String\">%name%</Data></Cell>\n" +
                    "   </Row>\n";
            }
        });


        /**
         * Заполнить шаблон данными
         * @param data
         */
        ExcelT1.prototype.fillTemplate = function (data) {
            this.content = this.template;

            this.fillProperty("lastName", data.lastName);
            this.fillProperty("firstName", data.firstName);
            this.fillProperty("middleName", data.middleName);
            this.fillProperty("dob", data.dob);
            this.fillProperty("age", data.age);
            this.fillProperty("docType", data.docType);
            this.fillProperty("passportNumber", data.passportNumber);
            this.fillProperty("birthNumber", data.birthNumber);
            this.fillProperty("issueDate", data.issueDate);
            this.fillProperty("issuedBy", data.issuedBy);

            var eduContent = "";
            data.education.forEach(function (ed) {
                eduContent += this.educationTableTemplate;
                eduContent = eduContent.replace("%start%", ed.start);
                eduContent = eduContent.replace("%end%", ed.end);
                eduContent = eduContent.replace("%name%", ed.name);
            }.bind(this));

            this.fillProperty("educationTable", eduContent);

            var ExpandedRowCount= (15 + data.education.length*3) + "";
            this.content = this.content.replace("ss:ExpandedRowCount=\"18\"", "ss:ExpandedRowCount=\"" + ExpandedRowCount + "\"");
        };


        /**
         * Заполнить переменную в шаблоне
         * @param propName
         * @param value
         */
        ExcelT1.prototype.fillProperty = function (propName, value) {
            this.content = this.content.replace("%" + propName + "%", value);
        };


        return ExcelT1;


    });