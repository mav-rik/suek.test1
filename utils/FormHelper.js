sap.ui.define(['sap/ui/base/Object', 'sap/m/Label', 'sap/m/Input',
        'sap/m/Select', 'sap/m/DatePicker', 'sap/m/Toolbar', 'sap/m/ToolbarSpacer',
        'sap/ui/core/Title', 'sap/m/Title', 'sap/m/Button'],
    function (Object, Label, Input, Select, DatePicker, Toolbar, ToolbarSpacer, CoreTitle, Title, Button) {

        /**
         * @class Form Helper содержит методы для наполнения форм контролами
         * @extends sap.ui.base.Object
         * @author Artem Maltsev
         * @public
         * @alias suek.test1.utils.FormHelper
         */
        var FormHelper = Object.extend("suek.test1.utils.FormHelper", {
            constructor: function(dataPath) {
                this._dataPath = dataPath;
            }
        });

        /**
         * Создать заголовок для формы (если с кнопками, то в виде тулбара)
         * @param name
         * @param buttons
         * @returns {*}
         */
        FormHelper.prototype.createTitle = function (name, buttons) {
            if (buttons && buttons.length) {
                var aButtons = [];
                buttons.forEach(function(data) {
                    aButtons.push(new Button(data))
                });
                return new Toolbar({
                    content: [
                        new Title({text: "{i18n>" + name + "}", level: "H5", titleStyle: "H5"}),
                        new ToolbarSpacer({}),
                    ].concat(aButtons)
                });
            } else {
                return new CoreTitle({text: "{i18n>" + name + "}"})
            }
        };

        /**
         * Создать лэйбл для поля формы
         * @param name
         * @param required
         * @returns {*}
         */
        FormHelper.prototype.createLabel = function (name, required) {
            return new Label({
                text: "{i18n>" + name + "}",
                required: required
            })
        };

        FormHelper.prototype.createText = function (data) {
            var path = this._dataPath + (data.modelPrepath ? data.modelPrepath : "") +  data.model;
            var control = new sap.m.Text({
                text: {
                    path: path,
                    type: "sap.ui.model.type.String",
                    constraints: data.constraints
                }
            });
            return control;
        };

        /**
         * Создать поле определенного типа
         * @param data
         * @param callback
         * @returns {*}
         */
        FormHelper.prototype.createField = function (data, callback) {
            var path = this._dataPath + (data.modelPrepath ? data.modelPrepath : "") +  data.model;
            console.log(path);
            var control;
            switch (data.type) {
                case "Date":
                    control = new DatePicker({
                        value: {
                            path: path,
                            type: "sap.ui.model.type.Date",
                            constraints: data.constraints,
                            formatOptions: {
                                source: "yyyyMMdd"
                            }
                        },
                        enabled: !data.disabled
                    });
                    break;
                case "Select":
                    control = new Select({
                        selectedKey: "{" + path + "}",
                        forceSelection: true,
                        enabled: !data.disabled
                    });
                    control.bindItems(data.source, new sap.ui.core.Item({key: "{key}", text: "{text}"}));
                    break;
                default:
                    control = new Input({
                        value: {
                            path: path,
                            type: "sap.ui.model.type." + data.type,
                            constraints: data.constraints
                        },
                        required: data.required,
                        enabled: !data.disabled,
                        maxLength: data.maxLength || 0,
                        valueLiveUpdate: true
                    });
                    break;
            }
            if (callback) {
                control.attachChange(function(oEvent) {
                    callback({
                        field: data,
                        event: oEvent
                    });
                });
            }
            return control;
        };

        /**
         * Создать маленькую кнопку (в конце строки формы)
         * @param data
         * @returns {*}
         */
        FormHelper.prototype.createSmallButton = function(data) {
            return new Button(data).setLayoutData(new sap.ui.layout.GridData({span: "L1 M1 S1"}));
        };

        return FormHelper;

    });