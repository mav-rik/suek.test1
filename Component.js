sap.ui.define(['sap/ui/core/UIComponent'],
    function(UIComponent) {
    "use strict";

        var Component = UIComponent.extend("suek.test1.Component", {
            metadata : {

                manifest: "json"

            },

            init: function () {
                UIComponent.prototype.init && UIComponent.prototype.init.apply(this, arguments);

                this.getModel().setData({
                    data: {
                        lastName: "",
                        firstName: "",
                        middleName: "",
                        dob: "",
                        age: "",
                        docType: 0,
                        passportNumber: "",
                        birthNumber: "",
                        issuedBy: "",
                        issueDate: "",
                        education: [
                        ]
                    },
                    docTypes: [
                        {
                            key: 0,
                            text: "Паспорт",
                            fields: ["docType", "passportNumber", "issuedBy", "issueDate"]
                        },
                        {
                            key: 1,
                            text: "Свидетельство о рождении",
                            fields: ["docType", "birthNumber", "issueDate"]
                        }
                    ]
                });

                this.getRouter().initialize();

            }

        });

        return Component;

    });