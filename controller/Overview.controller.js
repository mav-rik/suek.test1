sap.ui.define(['sap/ui/core/mvc/Controller', 'suek/test1/model/Fields', 'suek/test1/utils/FormHelper',
        'suek/test1/utils/PDFHelper', 'suek/test1/utils/ExcelT1'],
    function (Controller, Fields, FormHelper, PDFHelper, ExcelT1) {
        return Controller.extend("suek.test1.controller.Overview", {

            /**
             * Инициализация контроллера
             * Подписываемся на событие роутера, инициализируем api с данными и хелпер формы
             */
            onInit: function () {

                this.getRouter().getRoute("overview").attachMatched(this.onRouteMatched.bind(this));
                this.fields = new Fields();
                this.formHelper = new FormHelper('/data/');

            },

            /**
             * Возвращает роутер
             * @returns {*}
             */
            getRouter: function () {
                return sap.ui.core.UIComponent.getRouterFor(this);
            },


            /**
             * Хендлер события роутера
             * Читаем данные и обновляем форму
             */
            onRouteMatched: function (oEvent) {
                this.fields.read().then(function(data) {
                    this.getView().getModel().setProperty("/fields", data);
                    this.refreshForm();
                }.bind(this))
            },


            /**
             * Обновление формы
             * Читаем данные из модели и на их основе строим форму
             */
            refreshForm: function () {
                var form = this.getView().byId('DataFormId');
                var oModel = this.getView().getModel();
                var fields = oModel.getProperty("/fields");
                var data = oModel.getProperty("/data");
                var docTypes = oModel.getProperty("/docTypes");
                if (!form)
                    return;
                form.removeAllContent();

                form.addContent(this.formHelper.createTitle("name"));
                for (var field in fields["name"]) {
                    form.addContent(this.formHelper.createLabel(field));
                    form.addContent(this.formHelper.createText(fields["name"][field]))
                }


                form.addContent(this.formHelper.createTitle("document"));
                var docType = docTypes.find(function(dt) {
                    return dt.key == data.docType
                });

                docType.fields.forEach(function(field) {
                    form.addContent(this.formHelper.createLabel(field));
                    form.addContent(this.formHelper.createText(fields["document"][field]))
                }.bind(this));


                form.addContent(this.formHelper.createTitle("education"));
                data.education.forEach(function(ed, i) {
                    for (var field in fields["education"]) {
                        form.addContent(this.formHelper.createLabel(field));
                        fields["education"][field].modelPrepath = "education/" + i + "/";
                        form.addContent(this.formHelper.createText(fields["education"][field]))
                    }
                }.bind(this))

            },

            /**
             * Экспорт в PDF
             */
            onPDF: function () {
                var oModel = this.getView().getModel();
                var fields = oModel.getProperty("/fields");
                var data = oModel.getProperty("/data");
                var docTypes = oModel.getProperty("/docTypes");
                new PDFHelper(data, fields, docTypes, this.getView().getModel('i18n')).save('data');
            },

            /**
             * Экспорт в Excel
             */
            onExcel: function () {
                var oModel = this.getView().getModel();
                var data = oModel.getProperty("/data");
                new ExcelT1().generateAndSave(data, 'data');
            }

        });
    });