sap.ui.define(['sap/ui/core/mvc/Controller', 'suek/test1/model/Fields', 'suek/test1/utils/FormHelper', 'sap/m/MessageBox'],
    function (Controller, Fields, FormHelper, MessageBox) {
        return Controller.extend("suek.test1.controller.Main", {
            /**
             * Инициализация контроллера
             * Получаем модель сообщений, регистрируем ракурс для модели сообщений,
             * подписываемся на событие роутера, инициализируем api с данными и хелпер формы
             */
            onInit: function () {

                var oMessageManager = sap.ui.getCore().getMessageManager();
                this.getView().setModel(oMessageManager.getMessageModel(), "message");

                oMessageManager.registerObject(this.getView(), true);

                this.getRouter().getRoute("home").attachMatched(this.onRouteMatched.bind(this));
                this.fields = new Fields();
                this.formHelper = new FormHelper('/data/');

            },

            /**
             * Возвращает роутер
             * @returns {*}
             */
            getRouter: function () {
                return sap.ui.core.UIComponent.getRouterFor(this);
            },

            /**
             * Хендлер события роутера
             * Читаем данные и обновляем форму
             */
            onRouteMatched: function () {
                this.fields.read().then(function(data) {
                    this.getView().getModel().setProperty("/fields", data);
                    this.refreshForm();
                }.bind(this))
            },

            /**
             * Обновление формы
             * Читаем данные из модели и на их основе строим форму
             */
            refreshForm: function () {
                var form = this.getView().byId('DataFormId');
                var oModel = this.getView().getModel();
                var fields = oModel.getProperty("/fields");
                var data = oModel.getProperty("/data");
                var docTypes = oModel.getProperty("/docTypes");
                if (!form)
                    return;
                form.removeAllContent();

                form.addContent(this.formHelper.createTitle("name"));
                for (var field in fields["name"]) {
                    form.addContent(this.formHelper.createLabel(field, fields["name"][field].required));
                    form.addContent(this.formHelper.createField(fields["name"][field], this.onFieldChange.bind(this)))
                }


                form.addContent(this.formHelper.createTitle("document"));
                var docType = docTypes.find(function(dt) {
                    return dt.key == data.docType
                });

                docType.fields.forEach(function(field) {
                    form.addContent(this.formHelper.createLabel(field));
                    form.addContent(this.formHelper.createField(fields["document"][field], this.onFieldChange.bind(this)))
                }.bind(this));


                form.addContent(this.formHelper.createTitle("education", [{
                    icon: "sap-icon://add",
                    press: this.onAddEducation.bind(this)
                }]));
                data.education.forEach(function(ed, i) {
                    for (var field in fields["education"]) {
                        form.addContent(this.formHelper.createLabel(field));
                        fields["education"][field].modelPrepath = "education/" + i + "/";
                        form.addContent(this.formHelper.createField(fields["education"][field], this.onFieldChange.bind(this)))
                    }
                    form.addContent(this.formHelper.createSmallButton({
                        icon: "sap-icon://delete",
                        text: "{education/" + i + "}",
                        press: this.onRemoveEducation.bind(this)
                    }));
                }.bind(this))


            },


            /**
             * Обработчик события изменения поля формы
             * Вычисляем возраст по дате рождения и обновляем форму
             * @param data
             */
            onFieldChange: function (data) {
                var oModel = this.getView().getModel();
                if (data.field.model === 'dob') {
                    var date = data.event.getSource().getDateValue();
                    oModel.setProperty('/data/age', date ? calcAge(date) : "" );
                } else if (data.field.model === 'docType') {
                    this.refreshForm();
                }

                function calcAge(birthday)
                {
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    return Math.abs(ageDate.getUTCFullYear() - 1970);
                 }
            },

            /**
             * Добавление полей для образования
             * @param oEvent
             */
            onAddEducation: function(oEvent) {
                var oModel = this.getView().getModel();
                var data = oModel.getProperty("/data");

                oModel.setProperty('/data/education/' + data.education.length, {
                    start: "",
                    end: "",
                    name: ""
                });
                this.refreshForm();
            },

            /**
             * Удаление полей для образования
             * @param oEvent
             */
            onRemoveEducation: function(oEvent) {
                var oModel = this.getView().getModel();
                var data = oModel.getProperty("/data");

                var pathSplitted = oEvent.getSource().getBinding("text").getPath().split("/");
                var index = pathSplitted[pathSplitted.length-1];
                data.education.splice(index,1);
                this.refreshForm();
            },

            /**
             * Переход на 2ю страницу с проверками
             */
            onSave: function () {
                var oModel = this.getView().getModel();
                var data = oModel.getProperty("/data");
                var oMessageModel = this.getView().getModel('message');
                if (oMessageModel.getData().length > 0) {
                    MessageBox.error('Исправьте ошибки ввода!');
                } else if (!data.lastName || !data.firstName){
                    MessageBox.error('Заполните все обязательные поля!');
                } else {
                   this.navigateToNextPage()
                }
            },

            /**
             * Навигация
             */
            navigateToNextPage: function () {
                this.getRouter().navTo('overview');
            }

        });
    });