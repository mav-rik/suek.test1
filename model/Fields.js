sap.ui.define(['sap/ui/base/Object'],
    function (Object) {

        /**
         * @class Fields выдает данные о полям в промисе
         * @extends sap.ui.base.Object
         * @author Artem Maltsev
         * @public
         * @alias suek.test1.model.Fields
         */
        var Fields = Object.extend("suek.test1.model.Fields", {
           constructor: function() {

           }
        });

        /**
         * Чтение данных (можно заменить на вызов api-сервера)
         * @returns {Promise<any>}
         */
        Fields.prototype.read = function () {
            return new Promise(function(resolve, reject) {
                resolve({
                    "name": {
                        "lastName": {type: "String", maxLength: 64, model: "lastName", constraints: {minLength: "1"}, required: true},
                        "firstName": {type: "String", maxLength: 64, model: "firstName", constraints: {minLength: "1"}, required: true},
                        "middleName": {type: "String", maxLength: 64, model: "middleName"},
                        "dob": {type: "Date", model: "dob", constraints: {maximum: "20150101", minimum: "19170101"}},
                        "age": {type: "String", model: "age", disabled: true}
                    },
                    "document": {
                        "docType": {type: "Select", maxLength: 64, model: "docType", source: "/docTypes/"},
                        "passportNumber": {type: "Integer", model: "passportNumber", maxLength: 10},
                        "birthNumber": {type: "Integer", model: "birthNumber", maxLength: 10},
                        "issuedBy": {type: "String", model: "issuedBy", maxLength: 128},
                        "issueDate": {type: "Date", model: "issueDate"},
                    },
                    "education": {
                        "start": {type: "Date", model: "start"},
                        "end": {type: "Date", model: "end"},
                        "eduName": {type: "String", model: "name", maxLength: 128}
                    }
                });
            })
        };

        return Fields;

    });